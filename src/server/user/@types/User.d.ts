declare type User = {
  name: string,
  fullID: string,
  id: string,
  lastActionTime: number
};
