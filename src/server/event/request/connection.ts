import SocketIO = require('socket.io');
import * as user from '../../user';
import * as responseEvent from '../../event/response';

export const connectionEvent = (
  socket: SocketIO.Socket,
  userMap: UserMap
): void => {
  console.log(`connection: ${socket.handshake.address}`);
  if (!socket.handshake.secure) {
    socket.emit('no_secure_alert', 'https で接続してください');
    socket.disconnect(true);
    return;
  }

  const userID = user.createUserID(socket.handshake.address);
  const newUser: User = {
    name: '名無しさん',
    fullID: userID,
    id: userID.slice(0, 11),
    lastActionTime: 0
  };
  userMap.set(socket.id, newUser);
  responseEvent.updateName(socket, newUser);
};
