import SocketIO = require('socket.io');

export const disconnectEvent = (
  socket: SocketIO.Socket,
  userMap: UserMap,
): void => {
  socket.on('disconnect', () => {
    if (!userMap.has(socket.id)) return;
    userMap.delete(socket.id);

    console.log(`${socket.handshake.address} is disconnected`);
    socket.disconnect(true);
  });
};
