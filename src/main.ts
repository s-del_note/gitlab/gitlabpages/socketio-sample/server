#! /usr/bin/env node

import fs = require('fs');
import { createServer } from './server/server';

(() => {
  const PORT = Number(process.env.PORT) || 3000;
  const KEY = process.env.KEY;
  const CERT = process.env.CERT;

  if (!PORT) throw new Error();
  if (!KEY) throw new Error();
  if (!CERT) throw new Error();

  const keyBuffer = fs.readFileSync(KEY);
  const certBuffer = fs.readFileSync(CERT);
  const server = createServer(keyBuffer, certBuffer);
  if (server.listening) throw new Error();

  server.listen(PORT);
  console.log(`server is Listening: PORT => ${PORT}`);
})();
